from django.contrib import admin
from django.urls import path

admin.site.site_header = 'Admin panel'
admin.site.site_title = 'Admin panel'

urlpatterns = [
    path('', admin.site.urls),
]
