from config.settings.general import *
from config.settings.apps import *

# CORS
CORS_ALLOW_ALL_ORIGINS = True
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOWED_ORIGINS = ["http://localhost:8000"]

# CORS_ORIGIN_WHITELIST = ('http://localhost:8000',)

CORS_ALLOW_METHODS = [
    'GET',
    'PUT',
    'POST'
]

CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
]
