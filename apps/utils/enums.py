from django.db.models import TextChoices
from django.utils.translation import ugettext as _


class State(TextChoices):
    ACTIVE = 'active', _('Активный')
    INACTIVE = 'inactive', _('Неактивный')
