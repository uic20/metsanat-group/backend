from django_filters import rest_framework as filters

from .models import Application, Student, StudentDetail


class ApplicationFilter(filters.FilterSet):
    min_date = filters.DateFilter(field_name="created_at", lookup_expr='gte')
    max_date = filters.DateFilter(field_name="created_at", lookup_expr='lte')


    class Meta:
        model = Application
        fields = ['status', 'price', 'min_date', 'max_date']


class StudentFilter(filters.FilterSet):
    class Meta:
        model = Student
        fields = ['education', 'student_type']


class StudentDetailFilter(filters.FilterSet):
    class Meta:
        model = StudentDetail
        fields = ['student', 'application', ]
