from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class MainConfig(AppConfig):
    name = 'metsenat'
    verbose_name =  _('Metsenats')

    def ready(self):
        import metsenat.signals
