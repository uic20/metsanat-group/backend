from django.urls import path

from .views import ApplicationList, ApplicationCreate, ApplicationDetail, StudentView, StudentViewDetail, \
    StudentDetailView, StudentDetailViewDetail

urlpatterns = [
    path('application', ApplicationList.as_view(), name='application_list'),
    path('application/', ApplicationCreate.as_view(), name='applications_create'),
    path('application/<int:pk>', ApplicationDetail.as_view(), name='application_details'),
    path('student', StudentView.as_view(), name='student_view'),
    path('student/<int:pk>', StudentViewDetail.as_view(), name='student_view_detail'),
    path('student/detail', StudentDetailView.as_view(), name='student_detail_view'),
    path('student/detail/<int:pk>', StudentDetailViewDetail.as_view(), name='student_detail_view_detail'),
]
