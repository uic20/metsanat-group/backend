from django.http import Http404
from rest_framework.response import Response
from rest_framework import status, permissions, generics
from rest_framework.views import APIView
from django_filters import rest_framework as filters
from .models import Application, Student, StudentDetail
from .serializers import ApplicationSerializer, StudentSerializer, StudentDetailSerializer
from .filters import ApplicationFilter, StudentFilter, StudentDetailFilter
from apps.utils.enums import State


class ApplicationList(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = Application.objects.filter(state=State.ACTIVE)
    serializer_class = ApplicationSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ApplicationFilter


class ApplicationCreate(generics.CreateAPIView):
    permission_classes = [permissions.AllowAny, ]
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


class ApplicationDetail(APIView):
    permission_classes = [permissions.IsAuthenticated, ]

    def get_object(self, pk):
        try:
            return Application.objects.get(pk=pk)
        except Application.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        application = self.get_object(pk)
        serializer = ApplicationSerializer(application)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        application = self.get_object(pk)
        if application.created_by:
            application.updated_by = request.user
        else:
            application.created_by = request.user
        serializer = ApplicationSerializer(application, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StudentView(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = Student.objects.filter(state=State.ACTIVE)
    serializer_class = StudentSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = StudentFilter


class StudentViewDetail(APIView):
    permission_classes = [permissions.IsAuthenticated, ]

    def get_object(self, pk):
        try:
            return Student.objects.get(pk=pk)
        except Student.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        student = self.get_object(pk)
        serializer = StudentSerializer(student)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        student = self.get_object(pk)
        if student.created_by:
            student.updated_by = request.user
        else:
            student.created_by = request.user
        serializer = StudentSerializer(student, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StudentDetailView(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = StudentDetail.objects.all()
    serializer_class = StudentDetailSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = StudentDetailFilter


class StudentDetailViewDetail(APIView):
    permission_classes = [permissions.IsAuthenticated, ]

    def get_object(self, pk):
        try:
            return StudentDetail.objects.get(pk=pk)
        except StudentDetail.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        student_detail = self.get_object(pk)
        serializer = StudentDetailSerializer(student_detail)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        student_detail = self.get_object(pk)
        if student_detail.created_by:
            student_detail.updated_by = request.user
        else:
            student_detail.created_by = request.user
        serializer = StudentDetailSerializer(student_detail, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
