from django.db import models
from .enums import PersonType, ApplicationType
from apps.utils.models import BaseModel
from django.utils.translation import ugettext as _


class Application(BaseModel):
    fullname = models.CharField(max_length=255)
    sender_id = models.IntegerField(default=0)
    phone_number = models.CharField(max_length=20)
    price = models.IntegerField(default=0)
    spent = models.IntegerField(default=0)
    org_name = models.CharField(max_length=255, null=True)
    pay_type = models.ForeignKey('references.PayType', models.SET_NULL, null=True, blank=True)
    type = models.CharField(max_length=10, choices=PersonType.choices, default=PersonType.PYSICAL)
    status = models.CharField(max_length=10, choices=ApplicationType.choices, default=ApplicationType.NEW)

    def __str__(self):
        return str(self.pk)

    class Meta:
        ordering = ['created_at']
        verbose_name = _('Application')
        verbose_name_plural = _('Applications')
        db_table = 'metsenat_applications'


class Student(BaseModel):
    fullname = models.CharField(max_length=255)
    sender_id = models.IntegerField(default=0)
    phone_number = models.CharField(max_length=20)
    contract_sum = models.IntegerField(default=0)
    devide_sum = models.IntegerField(default=0)
    education = models.ForeignKey('references.Education', on_delete=models.CASCADE)
    student_type = models.ForeignKey('references.StudentType', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)

    class Meta:
        ordering = ['created_at']
        verbose_name = _('Student')
        verbose_name_plural = _('Students')
        db_table = 'metsenat_students'


class StudentDetail(BaseModel):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    devide_sum = models.IntegerField(default=0)

    def __str__(self):
        return str(self.pk)

    class Meta:
        ordering = ['created_at']
        verbose_name = _('StudentDetail')
        verbose_name_plural = _('StudentDetails')
        db_table = 'metsenat_student_details'
