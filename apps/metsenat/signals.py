from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import StudentDetail, Application, Student


@receiver(post_save, sender=StudentDetail)
def set_change_spent(sender, instance, created, **kwargs):
    # Sposor applications spent sum
    application_id = int(str(instance.application))
    application = Application.objects.get(pk=application_id)
    application.spent = StudentDetail.objects.filter(application=application_id).aggregate(Sum('devide_sum')) \
        .get('devide_sum__sum', 0)
    application.save()

    # Students devide sum
    student_id = int(str(instance.student))
    student = Student.objects.get(pk=student_id)
    student.devide_sum = StudentDetail.objects.filter(student=student_id).aggregate(Sum('devide_sum')).get(
        'devide_sum__sum', 0)
    student.save()
