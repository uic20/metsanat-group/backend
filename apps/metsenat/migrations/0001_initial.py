# Generated by Django 3.2.5 on 2022-01-22 16:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('references', '0002_auto_20220121_1719'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Время создания')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='Время обновления')),
                ('state', models.CharField(choices=[('active', 'Активный'), ('inactive', 'Неактивный')], default='active', max_length=10)),
                ('fullname', models.CharField(max_length=255)),
                ('sender_id', models.IntegerField(default=0)),
                ('phone_number', models.CharField(max_length=20)),
                ('price', models.IntegerField(default=0)),
                ('spent', models.IntegerField(default=0)),
                ('org_name', models.CharField(max_length=255, null=True)),
                ('type', models.CharField(choices=[('legal', 'Legal'), ('pysical', 'Pysical')], default='pysical', max_length=10)),
                ('status', models.CharField(choices=[('new', 'New'), ('moderator', 'Moderator'), ('accept', 'Accept'), ('cancel', 'Cancel')], default='new', max_length=10)),
                ('created_by', models.ForeignKey(blank=True, db_column='created_by', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_applications', to=settings.AUTH_USER_MODEL, verbose_name='Создано пользоветем')),
                ('pay_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='references.paytype')),
                ('updated_by', models.ForeignKey(blank=True, db_column='updated_by', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='updated_applications', to=settings.AUTH_USER_MODEL, verbose_name='Обновлено пользоветем')),
            ],
            options={
                'verbose_name': 'Application',
                'verbose_name_plural': 'Applications',
                'db_table': 'metsenat_applications',
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Время создания')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='Время обновления')),
                ('state', models.CharField(choices=[('active', 'Активный'), ('inactive', 'Неактивный')], default='active', max_length=10)),
                ('fullname', models.CharField(max_length=255)),
                ('sender_id', models.IntegerField(default=0)),
                ('phone_number', models.CharField(max_length=20)),
                ('contract_sum', models.IntegerField(default=0)),
                ('devide_sum', models.IntegerField(default=0)),
                ('created_by', models.ForeignKey(blank=True, db_column='created_by', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_students', to=settings.AUTH_USER_MODEL, verbose_name='Создано пользоветем')),
                ('education', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='references.education')),
                ('student_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='references.studenttype')),
                ('updated_by', models.ForeignKey(blank=True, db_column='updated_by', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='updated_students', to=settings.AUTH_USER_MODEL, verbose_name='Обновлено пользоветем')),
            ],
            options={
                'verbose_name': 'Student',
                'verbose_name_plural': 'Students',
                'db_table': 'metsenat_students',
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='StudentDetail',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Время создания')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='Время обновления')),
                ('state', models.CharField(choices=[('active', 'Активный'), ('inactive', 'Неактивный')], default='active', max_length=10)),
                ('devide_sum', models.IntegerField(default=0)),
                ('application', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='metsenat.application')),
                ('created_by', models.ForeignKey(blank=True, db_column='created_by', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_studentdetails', to=settings.AUTH_USER_MODEL, verbose_name='Создано пользоветем')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='metsenat.student')),
                ('updated_by', models.ForeignKey(blank=True, db_column='updated_by', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='updated_studentdetails', to=settings.AUTH_USER_MODEL, verbose_name='Обновлено пользоветем')),
            ],
            options={
                'verbose_name': 'StudentDetail',
                'verbose_name_plural': 'StudentDetails',
                'db_table': 'metsenat_student_details',
                'ordering': ['created_at'],
            },
        ),
    ]
