from django.db.models import TextChoices
from django.utils.translation import ugettext as _


class PersonType(TextChoices):
    LEGAL = 'legal', _('Legal')
    PYSICAL = 'pysical', _('Pysical')


class ApplicationType(TextChoices):
    NEW = 'new', _('New')
    MODERATOR = 'moderator', _('Moderator')
    ACCEPT = 'accept', _('Accept')
    CANCEL = 'cancel', _('Cancel')
