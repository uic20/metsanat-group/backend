from rest_framework import serializers
from .enums import PersonType
from .models import Application, Student, StudentDetail


class ApplicationSerializer(serializers.ModelSerializer):
    def validate(self, data):
        try:
            if data['price'] < data['spent']:
                raise serializers.ValidationError({"error": "MAX_SPENT"})
            elif data['type'] == PersonType.LEGAL and data['org_name'] is None:
                raise serializers.ValidationError({"org_name": "This field is required."})
        except KeyError:
            raise serializers.ValidationError({"org_name": "This field is required."})
        return data

    class Meta:
        model = Application
        fields = (
            'id', 'fullname', 'sender_id', 'phone_number', 'price', 'spent', 'org_name',
            'pay_type', 'type', 'state', 'status',
            'created_at', 'updated_at', 'created_by', 'updated_by')
        extra_kwargs = {'fullname': {'required': True}, 'sender_id': {'required': True},
                        'phone_number': {'required': True}, 'price': {'required': True}, 'type': {'required': True},
                        'pay_type': {'required': True}, 'spent': {'required': True}}


class StudentSerializer(serializers.ModelSerializer):
    def validate(self, data):
        if data['contract_sum'] < data['devide_sum']:
            raise serializers.ValidationError({"error": "MAX_DEVIDE"})
        return data

    class Meta:
        model = Student
        fields = (
            'id', 'fullname', 'sender_id', 'phone_number', 'contract_sum', 'devide_sum',
            'education', 'student_type', 'state',
            'created_at', 'updated_at', 'created_by', 'updated_by')
        extra_kwargs = {'fullname': {'required': True}, 'sender_id': {'required': True},
                        'phone_number': {'required': True}, 'contract_sum': {'required': True},
                        'education': {'required': True}, 'student_type': {'required': True},
                        'devide_sum': {'required': True}}


class StudentDetailSerializer(serializers.ModelSerializer):

    def validate(self, data):
        application_id = int(str(data['application']))
        devide_sum = int(str(data['devide_sum']))
        student_id = int(str(data['student']))

        application = Application.objects.get(pk=application_id)
        application_devide = application.price - application.spent

        student = Student.objects.get(pk=student_id)
        spent_sum = student.contract_sum - student.devide_sum

        if (application_devide < devide_sum):
            raise serializers.ValidationError({"error": "APPLICATION", "devide": application_devide})
        elif (devide_sum > spent_sum):
            raise serializers.ValidationError({"error": "STUDENT", "devide": spent_sum})
        return data

    class Meta:
        model = StudentDetail
        fields = (
            'id', 'student', 'application', 'devide_sum', 'state',
            'created_at', 'updated_at', 'created_by', 'updated_by')
        extra_kwargs = {'student': {'required': True}, 'application': {'required': True},
                        'devide_sum': {'required': True}}
