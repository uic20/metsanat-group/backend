from django.contrib import admin

# Register your models here.
from .models import Education, StudentType, PayType


@admin.register(Education)
class EducationAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'sort_order']
    list_display_links = ['name']
    fields = ['name', 'sort_order']


@admin.register(StudentType)
class StudentTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'sort_order']
    list_display_links = ['name']
    fields = ['name', 'sort_order']


@admin.register(PayType)
class PayTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'sort_order']
    list_display_links = ['name']
    fields = ['name', 'sort_order']
