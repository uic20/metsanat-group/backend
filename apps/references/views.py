from rest_framework import permissions, generics
from .models import Education, StudentType, PayType
from .serializers import EducationSerializer, StudentTypeSerializer, PayTypeSerializer


class EducationView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Education.objects.all()
    serializer_class = EducationSerializer


class StudentTypeView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = StudentType.objects.all()
    serializer_class = StudentTypeSerializer


class PayTypeView(generics.ListAPIView):
    #permission_classes = [permissions.IsAuthenticated]
    queryset = PayType.objects.all()
    serializer_class = PayTypeSerializer
