from django.db import models
from django.utils.translation import ugettext as _

# Create your models here.
from utils.models import BaseModel


class Education(BaseModel):
    name = models.CharField(max_length=100, blank=True, verbose_name='name')
    sort_order = models.IntegerField(blank=True, verbose_name='number')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['sort_order']
        verbose_name = _('Education')
        verbose_name_plural = _('Educations')
        db_table = 'reference_educations'


class StudentType(BaseModel):
    name = models.CharField(max_length=100, blank=True, verbose_name='name')
    sort_order = models.IntegerField(blank=True, verbose_name='number')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['sort_order']
        verbose_name = _('StudentType')
        verbose_name_plural = _('StudentTypes')
        db_table = 'reference_student_types'


class PayType(BaseModel):
    name = models.CharField(max_length=100, blank=True, verbose_name='name')
    sort_order = models.IntegerField(blank=True, verbose_name='number')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['sort_order']
        verbose_name = _('PayType')
        verbose_name_plural = _('PayTypes')
        db_table = 'reference_pay_types'
