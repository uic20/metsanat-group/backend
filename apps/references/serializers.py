from rest_framework import serializers
from .models import Education, StudentType, PayType


class EducationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Education
        fields = ('id', 'name', 'sort_order')


class StudentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentType
        fields = ('id', 'name', 'sort_order')


class PayTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PayType
        fields = ('id', 'name', 'sort_order')
