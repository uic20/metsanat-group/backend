from django.urls import path

from .views import EducationView, PayTypeView, StudentTypeView

urlpatterns = [
    path('educations', EducationView.as_view(), name='education_list'),
    path('pay_types', PayTypeView.as_view(), name='pay_type_list'),
    path('student_types', StudentTypeView.as_view(), name='student_type_list'),
]
