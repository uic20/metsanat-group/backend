def extract_emails(filename):
    emails = []
    filename = filename + ".txt" if filename else "mbox-short.txt"

    with open(filename) as file:
        for line in file.readlines():
            if line.strip().lower().startswith("from: "):
                # тут ниже разбиваем строку на части по пробелам            -- .split(' '),
                # потом берем второй по счету элемент это сам адрес почты   --  [1]
                # в адресе удаляем ненужный нам символ переноса строки      -- .replace("\n", "")
                email = line.split(' ')[1].replace("\n", "")
                emails.append(email)

    return emails


result = extract_emails(input("Введите название файла: "))
print('Общее количество электронных почтовых адресов: ', len(result))
print('Найденные почтовые адреса: ')

for address in result:
    print(address)
